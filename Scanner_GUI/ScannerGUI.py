__author__ = 'ihwang'

from Tkinter import *

channel_list = [" Channel1  ", " Channel2  ", " Channel3  ", " Channel4  ", " Channel5  ", " Channel6  ", " Channel7  ", " Channel8  ", " Channel9  ", " Channel10  "]

#1: free(available)
#0: occupied
channel_status = [ 1, 0, 1, 0, 1, 0, 0, 1, 1, 0]

class ScannerGUI:
    def __init__(self, parent, channels):
        self.channels = channel_list

        self.myParent = parent
        self.myParent.geometry("580x120")

        self.myContainer1 = Frame(parent)
        self.myContainer1.pack(expand=YES, fill=BOTH)

        self.channels_frame = Frame(self.myContainer1)
        self.channels_frame.pack(side=LEFT, expand=NO, fill=Y, ipadx=5, ipady=5)

        self.channel_list_canvas = Canvas(self.channels_frame, width=580, height=120)
        self.channel_list_canvas.pack()
        self.canning_channel_update = 0

    def channel_button_init(self):
        channel_index = 0
        for channel in self.channels:
            self.channel_list_canvas.create_text(20+channel_index*55, 30, anchor=W, font="Purisa 8", text=channel)
            self.channel_list_canvas.create_oval(28+channel_index*55, 52, 62+channel_index*55, 86, outline="white",fill="red", width=2)
            channel_index = channel_index +1


    # channel: channel index (1~10) free: 1 (available) 0(occupied)
    def channel_button_status_update(self, channel, free):
        channel_index = channel-1
        pos_offset = channel_index*50
        if free == 1:
            self.channel_list_canvas.create_oval(28+channel_index*55, 52, 62+channel_index*55, 86, outline="white",fill="green", width=2)
        else:
            self.channel_list_canvas.create_oval(28+channel_index*55, 52, 62+channel_index*55, 86, outline="white",fill="red", width=2)

    # update status of all channels in the list after scanning is done -   free: 1 (available) 0(occupied)
    def channel_list_status_update(self, channel_status_array):
        channel_index = 0
        for channel_status in channel_status_array:
            if channel_status == 1:
                self.channel_list_canvas.create_oval(28+channel_index*55, 52, 62+channel_index*55, 86, outline="white",fill="green", width=2)
            else:
                self.channel_list_canvas.create_oval(28+channel_index*55, 52, 62+channel_index*55, 86, outline="white",fill="red", width=2)
            channel_index = channel_index +1

def main():
    # instantiate a Tkinter object
    tk = Tk()
    tk.title("        Channel Scanner")
    tk.iconbitmap(default='scanner.ico')

    scannerGUI = ScannerGUI(tk, channel_list)
    scannerGUI.channel_button_init()

    #scannerGUI.channel_button_status_update(2,1)

    scannerGUI.channel_list_status_update(channel_status)

    tk.mainloop()

main()