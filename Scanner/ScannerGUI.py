__author__ = 'ihwang'

from Tkinter import *
import time

channel_list = [" Channel1  ", " Channel2  ", " Channel3  ", " Channel4  ", " Channel5  ", " Channel6  ", " Channel7  ", " Channel8  ", " Channel9  ", " Channel10  "]

class ScannerGUIApp:
    def __init__(self, parent, channels):
        self.channels = channel_list

        self.myParent = parent
        self.myParent.geometry("640x480")

        self.myContainer1 = Frame(parent)
        self.myContainer1.pack(expand=YES, fill=BOTH)

        self.channels_frame = Frame(self.myContainer1)
        self.channels_frame.pack(side=LEFT, expand=NO, fill=Y, ipadx=5, ipady=5)

        self.channel_list_canvas = Canvas(self.channels_frame, width=140, height=480)
        self.channel_list_canvas.pack()
        self.canning_channel_update = 0

        #self.top_frame = Frame(self.myContainer1)
        #self.top_frame.pack(side=TOP, expand=YES, fill=BOTH)

        self.right_top_frame = Frame(self.myContainer1)
        self.right_top_frame.pack(side=TOP, expand=YES, fill=BOTH)

        self.right_bottom_frame = Frame(self.myContainer1)
        self.right_bottom_frame.pack(side=RIGHT, expand=YES, fill=BOTH)


        #self.left_frame = Frame(self.top_frame,
        #                        background="red",
        #                      borderwidth=5,
        #                      relief=RIDGE,
        #                      width=50,
        #                      )
        #self.left_frame.pack(side=LEFT, expand=NO, fill=Y)

        self.title_frame = Frame(self.right_top_frame, borderwidth=5, relief=RIDGE, height=30, bg="cyan")
        self.title_frame.pack(side=TOP, fill=BOTH)

        self.spectrum_waveform_frame = Frame(self.right_bottom_frame, background="tan",borderwidth=5, relief=RIDGE, width=500, height = 440 )
        self.spectrum_waveform_frame.pack(side=TOP, expand=YES, fill=BOTH)


        #Label(self.button_names_frame, text="\nButton").pack()
        #Label(self.side_options_frame, text="Side\nOption").pack()
        #Label(self.fill_options_frame, text="Fill\nOption").pack()
        #Label(self.expand_options_frame, text="Expand\nOption").pack()
        Label(self.title_frame, text="Frequency Spectrum waveform\n", pady = 5, font="Purisa 11",  bg="cyan").pack()


    def channel_button_init(self):
        channel_index = 0
        for channel in self.channels:
            self.channel_list_canvas.create_oval(10, 10+channel_index*48, 40, 40+channel_index*48, outline="blue",fill="red", width=2)
            self.channel_list_canvas.create_text(50, 25+channel_index*48, anchor=W, font="Purisa 8", text=channel)
            channel_index = channel_index +1


    def channel_button_status_update(self, channel, free):
        channel_index = channel-1
        pos_offset = channel_index*50
        if free == 1:
            self.channel_list_canvas.create_oval(10, 10+channel_index*48, 40, 40+channel_index*48, outline="red",fill="green", width=2)
        else:
            self.channel_list_canvas.create_oval(10, 10+channel_index*48, 40, 40+channel_index*48, outline="blue",fill="red", width=2)


    def blinking_scanning_channel(self, channel):
        channel_index = channel-1
        pos_offset = channel_index*50
        if self.canning_channel_update == 0:
            self.channel_list_canvas.create_oval(10, 10+channel_index*48, 40, 40+channel_index*48, outline="red",fill="green", width=2)
            self.canning_channel_update = 1
        else:
            #self.channel_list_canvas.create_oval(10, 10+channel_index*48, 40, 40+channel_index*48, outline="blue",fill="red", width=2)
            self.channel_list_canvas.create_oval(10, 10+channel_index*48, 40, 40+channel_index*48, outline="red",fill="yellow", width=2)
            self.canning_channel_update = 0

def main():
    tk = Tk()
    tk.title("    Tx Channel Scanner")
    scannerGUI = ScannerGUIApp(tk, channel_list)
    scannerGUI.channel_button_init()
    scannerGUI.channel_button_status_update(2,1)
    scannerGUI.channel_button_status_update(7,1)
    #tk.mainloop()
    while 1:
        tk.update_idletasks()
        tk.update()
        scannerGUI.blinking_scanning_channel(8)
        time.sleep(0.5)

main()