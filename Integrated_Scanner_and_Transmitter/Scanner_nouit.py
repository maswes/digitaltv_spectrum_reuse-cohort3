#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Top Block
# Generated: Mon Apr 27 23:00:36 2015
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import math
import time
import logging
import sys
import threading
import os
from Queue import Queue

    
channelqueue = Queue([10])
LOG_FILENAME = 'example.log'
MAX_CHANNELS = 10
FILE_DIR = "./temp/"
CHAN_BASE = 494210000
 # sample file name = /home/rvennam/gnuradio/debug/spectrum.dat"
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG,
                    format=' %(asctime)s (%(threadName)-10s) %(message)s',
                    )


import struct
THOLD = 0.0001
CLIST = [0,0,0,0,0,0,0,0,0]
DBG_EN = 0

def print_chanlist():
	for i in range(0,9):
	    fname = FILE_DIR + 'chan'+str(i)+'.dat'
	    inp = open(fname,'rb')
	    if DBG_EN:
		out = open('out_'+fname,'wb')
	    item_count = 0
	    sum = 0.0
	    while True:
		flt = inp.read(4)
		if not flt:
		    break
		item_count+=1
		(a) = struct.unpack('f',flt)
		#print a[0]
		if DBG_EN:
		    out.write(str(a[0])+"\n")
		sum = sum + abs(a[0])
#	    print "SUM of file " + str(i) + "   "+str(sum)
#	    print item_count
	    avg = sum/item_count
#            print str(THOLD)
#            print str(avg)
#            print "AVG of file " + str(i) + "   "+str(avg)
	    if avg > THOLD:
		CLIST[i] = 1

                print "Channel" + "   "+str(i) + "   "+"is occupied"




	    else:
		CLIST[i] = 0
                print "Channel" + "   " + str(i) + "   "+"is free"
                inp.close()
	   # out.close()
	print " Final list of all channels 1 represents occupied and 0 represents free channel: " + "  " + str(CLIST)





class top_block(gr.top_block):

    def __init__(self, address="addr=192.168.11.2"):
        gr.top_block.__init__(self, "Top Block")

    ##################################################
    # Parameters
    ##################################################
	self.address = address

        ##################################################
        # Variables
        ##################################################
	self.samp_rate = samp_rate = 400000
	self.center_freq = center_freq = 484210000

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join((address, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_center_freq(center_freq, 0)
        self.uhd_usrp_source_0.set_gain(20, 0)
        self.uhd_usrp_source_0.set_antenna("RX2", 0)
        self.uhd_usrp_source_0.set_bandwidth(samp_rate, 0)
        self.blocks_sub_xx_0 = blocks.sub_ff(1)
        self.blocks_file_sink_1 = blocks.file_sink(gr.sizeof_float*1, "/home/rvennam/gnuradio/debug/spectrum.dat", False)
        self.blocks_file_sink_1.set_unbuffered(False)
        self.blocks_complex_to_mag_squared_2 = blocks.complex_to_mag_squared(1)
        self.blocks_complex_to_mag_squared_1 = blocks.complex_to_mag_squared(1)
        self.band_pass_filter_0_0 = filter.fir_filter_ccf(20, firdes.band_pass(
        	100, samp_rate, 95000, 105000, 2000, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0 = filter.fir_filter_ccf(20, firdes.band_pass(
        	100, samp_rate, 1000, 11000, 2000, firdes.WIN_HAMMING, 6.76))

        ##################################################
        # Connections
        ##################################################
        self.connect((self.uhd_usrp_source_0, 0), (self.band_pass_filter_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.band_pass_filter_0_0, 0))
        self.connect((self.band_pass_filter_0, 0), (self.blocks_complex_to_mag_squared_1, 0))
        self.connect((self.band_pass_filter_0_0, 0), (self.blocks_complex_to_mag_squared_2, 0))
        self.connect((self.blocks_complex_to_mag_squared_2, 0), (self.blocks_sub_xx_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_1, 0), (self.blocks_sub_xx_0, 1))
        self.connect((self.blocks_sub_xx_0, 0), (self.blocks_file_sink_1, 0))



    def get_address(self):
        return self.address

    def set_address(self, address):
        self.address = address

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.band_pass_filter_0.set_taps(firdes.band_pass(100, self.samp_rate, 1000, 11000, 2000, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_0.set_taps(firdes.band_pass(100, self.samp_rate, 95000, 105000, 2000, firdes.WIN_HAMMING, 6.76))
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_bandwidth(self.samp_rate, 0)

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.uhd_usrp_source_0.set_center_freq(self.center_freq, 0)

    def get_file_sink(self):
        return self.file_sink

    def set_file_sink(self, file_sink):
        self.file_sink = file_sink
        self.blocks_file_sink_1.open(self.file_sink)

if __name__ == '__main__':
    import ctypes
    import sys
    
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    (options, args) = parser.parse_args()
    
    logging.debug('Launching the scanner group')
    tb = top_block()
    
    if not os.path.exists(FILE_DIR):
        os.makedirs(FILE_DIR)
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'

    print '\n'+ "Scanner is scanning all TV UHF channels" + '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'

    for i in range(MAX_CHANNELS):
        logging.debug('Configuring the scanner for chnnale : %d',i)
        fpath_chan = FILE_DIR + "chan" + str(i) + ".dat"    
        chan_freq =  CHAN_BASE + i*6000000 
        tb.set_file_sink(fpath_chan)           
        tb.set_center_freq(chan_freq)    
        logging.debug('Scanning the channel : %d',i)
	logging.debug ('Channel center frequency : %f', tb.get_center_freq())       
        tb.start(True)        
        time.sleep(2) # Wait 2 secs (assuming sleep was imported!)
        tb.stop()
        tb.wait() # If the graph is needed to run again, wait() must be called after stop
        channelqueue.put(fpath_chan)
        logging.debug('Finished Scanner the channel : %d',i)

    #    close the flow graph
    tb.stop()
    logging.debug('Finished Scanner the channel : %d',i)
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    
    #print the statistics
    for i in range(MAX_CHANNELS):
        logging.debug('Channel names is : %s , file name is %s',i,channelqueue.get())
    
    # print the channelk list
    print_chanlist()
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'

    print (" Scanning is done!!! Exit and now let's transmitt of free channels ")
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'
    print '\n'

    ##quit()
    #system.exit("hai")
    #raise SystemExit()

#    try:
    sys.exit()
#    except SystemExit:
#	print ("sys.exit worked as expected")
#    except:
#	print ("someting is wrong")

    print ("we should reach here!!!!!")
    #sys.exit()        
    #os._exit(0)

