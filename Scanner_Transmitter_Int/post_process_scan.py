import struct
THOLD = 0.0001
CLIST = [0,0,0,0,0,0,0,0,0]
DBG_EN = 1


for i in range(0,9):
    fname = 'chan'+str(i)+'.dat'
    inp = open(fname,'rb')
    if DBG_EN:
        out = open('out_'+fname,'wb')
    item_count = 0
    sum = 0.0
    while True:
        flt = inp.read(4)
        if not flt:
            break
        item_count+=1
        (a) = struct.unpack('f',flt)
        #print a[0]
        if DBG_EN:
            out.write(str(a[0])+"\n")
        sum = sum + a[0]
    #print "SUM of file " + str(i) + str(
    #print item_count
    avg = sum/item_count
    if avg > THOLD:
        CLIST[i] = 1
    else:
        CLIST[i] = 0
    inp.close()
    out.close()
print CLIST
 