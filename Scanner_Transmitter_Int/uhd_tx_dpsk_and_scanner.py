#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: UHD TX DPSK
# Generated: Tue May 19 23:27:13 2015
##################################################

from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio import wxgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.wxgui import constsink_gl
from gnuradio.wxgui import forms
from grc_gnuradio import blks2 as grc_blks2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import time
import wx

class uhd_tx_dpsk(grc_wxgui.top_block_gui):

    def __init__(self, gain=0, samp_rate=1e6, address="addr=192.168.11.2", freq=550e6 + 10):
        grc_wxgui.top_block_gui.__init__(self, title="UHD TX DPSK")
        _icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
        self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

        ##################################################
        # Parameters
        ##################################################
        self.gain = gain
        self.samp_rate = samp_rate
        self.address = address
        self.freq = freq

        ##################################################
        # Variables
        ##################################################
        self.tun_gain = tun_gain = gain
        self.tun_freq = tun_freq = freq
        self.samps_per_sym = samps_per_sym = 4
        self.ampl = ampl = .1

        ##################################################
        # Blocks
        ##################################################
        _tun_gain_sizer = wx.BoxSizer(wx.VERTICAL)
        self._tun_gain_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_tun_gain_sizer,
        	value=self.tun_gain,
        	callback=self.set_tun_gain,
        	label="UHD Gain",
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._tun_gain_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_tun_gain_sizer,
        	value=self.tun_gain,
        	callback=self.set_tun_gain,
        	minimum=0,
        	maximum=20,
        	num_steps=100,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_tun_gain_sizer)
        _tun_freq_sizer = wx.BoxSizer(wx.VERTICAL)
        self._tun_freq_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_tun_freq_sizer,
        	value=self.tun_freq,
        	callback=self.set_tun_freq,
        	label="Freq (Hz)",
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._tun_freq_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_tun_freq_sizer,
        	value=self.tun_freq,
        	callback=self.set_tun_freq,
        	minimum=2.4e6,
        	maximum=2.5e9,
        	num_steps=100,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_tun_freq_sizer)
        _ampl_sizer = wx.BoxSizer(wx.VERTICAL)
        self._ampl_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_ampl_sizer,
        	value=self.ampl,
        	callback=self.set_ampl,
        	label="Amplitude",
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._ampl_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_ampl_sizer,
        	value=self.ampl,
        	callback=self.set_ampl,
        	minimum=0,
        	maximum=1,
        	num_steps=100,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_ampl_sizer)
        self.wxgui_constellationsink2_0 = constsink_gl.const_sink_c(
        	self.GetWin(),
        	title="Constellation Plot",
        	sample_rate=samp_rate,
        	frame_rate=5,
        	const_size=2048,
        	M=4,
        	theta=0,
        	loop_bw=6.28/100.0,
        	fmax=0.06,
        	mu=0.5,
        	gain_mu=0.005,
        	symbol_rate=samp_rate/4.,
        	omega_limit=0.005,
        )
        self.Add(self.wxgui_constellationsink2_0.win)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join((address, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_center_freq(tun_freq, 0)
        self.uhd_usrp_sink_0.set_gain(tun_gain, 0)
        self.uhd_usrp_sink_0.set_antenna("TX/RX", 0)
        self.digital_dxpsk_mod_0 = digital.dqpsk_mod(
        	samples_per_symbol=samps_per_sym,
        	excess_bw=0.35,
        	mod_code="gray",
        	verbose=False,
        	log=False)
        	
        self.blocks_vector_source_x_0_0 = blocks.vector_source_b((1,1,1,1,0,0,0,1,0,0,1,1,0,1,0), True, 1, [])
        self.blocks_vector_source_x_0 = blocks.vector_source_b((0,1,3,2), True, 1, [])
        self.blocks_stream_mux_0 = blocks.stream_mux(gr.sizeof_char*1, (64, 128, 1024))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((ampl*9, ))
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, "/home/rvennam/payload.bin", True)
        self.blks2_packet_encoder_0 = grc_blks2.packet_mod_b(grc_blks2.packet_encoder(
        		samples_per_symbol=4,
        		bits_per_symbol=2,
        		preamble="",
        		access_code="",
        		pad_for_usrp=True,
        	),
        	payload_length=0,
        )

        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.digital_dxpsk_mod_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_vector_source_x_0, 0), (self.blocks_stream_mux_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_stream_mux_0, 2))
        self.connect((self.blocks_vector_source_x_0_0, 0), (self.blocks_stream_mux_0, 1))
        self.connect((self.blks2_packet_encoder_0, 0), (self.digital_dxpsk_mod_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.wxgui_constellationsink2_0, 0))
        self.connect((self.blocks_stream_mux_0, 0), (self.blks2_packet_encoder_0, 0))



    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.set_tun_gain(self.gain)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.wxgui_constellationsink2_0.set_sample_rate(self.samp_rate)
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)

    def get_address(self):
        return self.address

    def set_address(self, address):
        self.address = address

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.set_tun_freq(self.freq)

    def get_tun_gain(self):
        return self.tun_gain

    def set_tun_gain(self, tun_gain):
        self.tun_gain = tun_gain
        self._tun_gain_slider.set_value(self.tun_gain)
        self._tun_gain_text_box.set_value(self.tun_gain)
        self.uhd_usrp_sink_0.set_gain(self.tun_gain, 0)

    def get_tun_freq(self):
        return self.tun_freq

    def set_tun_freq(self, tun_freq):
        self.tun_freq = tun_freq
        self._tun_freq_slider.set_value(self.tun_freq)
        self._tun_freq_text_box.set_value(self.tun_freq)
        self.uhd_usrp_sink_0.set_center_freq(self.tun_freq, 0)

    def get_samps_per_sym(self):
        return self.samps_per_sym

    def set_samps_per_sym(self, samps_per_sym):
        self.samps_per_sym = samps_per_sym

    def get_ampl(self):
        return self.ampl

    def set_ampl(self, ampl):
        self.ampl = ampl
        self._ampl_slider.set_value(self.ampl)
        self._ampl_text_box.set_value(self.ampl)
        self.blocks_multiply_const_vxx_0.set_k((self.ampl*9, ))

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    parser.add_option("-g", "--gain", dest="gain", type="eng_float", default=eng_notation.num_to_str(0),
        help="Set Default Gain [default=%default]")
    parser.add_option("-s", "--samp-rate", dest="samp_rate", type="eng_float", default=eng_notation.num_to_str(1e6),
        help="Set Sample Rate [default=%default]")
    parser.add_option("-a", "--address", dest="address", type="string", default="addr=192.168.11.2",
        help="Set IP Address [default=%default]")
    parser.add_option("-f", "--freq", dest="freq", type="eng_float", default=eng_notation.num_to_str(550e6 + 10),
        help="Set Default Frequency [default=%default]")
    (options, args) = parser.parse_args()
    tb = uhd_tx_dpsk(gain=options.gain, samp_rate=options.samp_rate, address=options.address, freq=options.freq)
    tb.Start(True)
    tb.Wait()
