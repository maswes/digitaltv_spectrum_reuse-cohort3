Please check the folder Development_Guide if you wish to jumpstart project using GNU radio and Software defined readio platform!

Each Folder has one text file which has info about codes and folder contents.

Project Final Integrated Folders

1. Integrated_Scanner_and_Transmitter: This has integrated scanner and frequency hopping transmitter model
2. Integrated_Scanner_and_Receiver: This has integrated scanner and receiver model

Project Intermediate folders 
1. Scanner: Implementation stand alone scanner
2. Transmitter: Implementation stand alone hopping transmitter
3. Receiver: Implementation stand alone receiver
4. TX_RX_Simulator: Simulated Transmit and Receiver with simulated channel 
5. Header_Payload_Separator: Post processing header and payload separator 
6. Scanner_GUI: GUI used to display output of scanner showing all empty and occupied channels

Project Plan folder
1. WeekelyPresentations

Final_Presentation 
1. This Folder has version 1 of Final presentation
2. We might have second vesrion by end of the day on 6/5/2015 with some more touchups 
3. We might also work on one more version as a release version
4. We will upload one video on youtube and share by end of the day 06/05/2015
5. Video uploaded on Youtube and shared with prof. http://youtu.be/zG5C2gWJaIA 


