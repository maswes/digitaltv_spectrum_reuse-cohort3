﻿1
00:00:19,000 --> 00:00:23,000
Project coconut conceived around these empty TV channels. 

2
00:00:23,000 --> 00:00:25,000
As wireless engineers we intended 

3
00:00:25,000 --> 00:00:28,000
to design a cognitive radio system 

4
00:00:28,000 --> 00:00:32,000
a smart system that scans for these empty channels and 

5
00:00:32,000 --> 00:00:34,000
reuse them to establishing peer to peer system 

6
00:00:34,000 --> 00:00:36,000
or broadcast communications system. 

7
00:00:43,000 --> 00:00:46,000
Let’s look at some use case, rescue Mission. 

8
00:00:46,000 --> 00:00:49,000
Imagine a scenario where we have wildfire 

9
00:00:49,000 --> 00:00:51,000
and all communications systems are collapsed. 

10
00:00:51,000 --> 00:00:54,000
Now controlling unit wants to pass simple 

11
00:00:54,000 --> 00:00:58,000
message "Move to North fire spreading in South". 

12
00:00:58,000 --> 00:01:02,000
Broadcast system established on empty TV 

13
00:01:02,000 --> 00:01:07,000
Channels effectively address this problem.

14
00:01:09,000 --> 00:01:11,000
One of the objective of the project is 

15
00:01:11,000 --> 00:01:13,000
to reuse technologies to make life simpler. 

16
00:01:13,000 --> 00:01:16,000
Retailers spend lot of money and paper on ads and coupons. 

17
00:01:16,000 --> 00:01:18,000
How about!! These coupons being downloaded 

18
00:01:18,000 --> 00:01:21,000
to your phone, the moment you step into the store.

19
00:01:23,000 --> 00:01:26,000
A cheap pet tracker which doesn't require any data plans. 

20
00:01:26,000 --> 00:01:28,000
Wouldn't it will be cool 

21
00:01:28,000 --> 00:01:30,000
if we could build it on empty TV channels? 

22
00:01:33,000 --> 00:01:35,000
Here is the block diagram of our system.

23
00:01:35,000 --> 00:01:39,000
we have scanner at the transmitter and at the receiver

24
00:01:39,000 --> 00:01:41,000
to detect empty TV channels. 

25
00:01:41,000 --> 00:01:44,000
We have frequency hopping transmitter which 

26
00:01:44,000 --> 00:01:47,000
uses empty channel list created by scanner.

27
00:01:47,000 --> 00:01:50,000
Then transmitter sends beacons 

28
00:01:50,000 --> 00:01:53,000
messages over each empty TV channel 

29
00:01:53,000 --> 00:01:57,000
we have Receiver which listens on these empty TV channels

30
00:02:00,000 --> 00:02:02,000
Here is the scanner design in detail. 

31
00:02:02,000 --> 00:02:06,000
Scanner estimates the energy level in each TV channel 

32
00:02:06,000 --> 00:02:08,000
and updates the empty channel list. 

33
00:02:11,000 --> 00:02:14,000
Differential bandpass filters are used to estimate 

34
00:02:14,000 --> 00:02:18,000
energy of the pilot of the video signal in each TV channel 

35
00:02:20,000 --> 00:02:25,000
Here is our scanner module in action green slot indicate 

36
00:02:25,000 --> 00:02:30,000
occupied channel and orange slot indicate empty channels

37
00:02:33,000 --> 00:02:35,000
Here are our scanner test results. 

38
00:02:35,000 --> 00:02:42,000
As you can see we are able detect 8 empty channels in 92126 area

39
00:02:49,000 --> 00:02:51,000
Here is the block digram of our 

40
00:02:51,000 --> 00:02:53,000
frequency hopping transmitter. 

41
00:02:53,000 --> 00:02:57,000
We do packet formation by adding PRBS header 

42
00:02:57,000 --> 00:03:04,000
encode each packet apply differential QPSK modulation 

43
00:03:04,000 --> 00:03:06,000
transmit for few seconds and then 

44
00:03:06,000 --> 00:03:10,000
hop on to the next empty TV channel and repeat. 

45
00:03:10,000 --> 00:03:15,000
Now let's see transmitter in action in next video. 

46
00:03:17,000 --> 00:03:20,000
Transmitter transmits message beacons 

47
00:03:20,000 --> 00:03:23,000
for 5 seconds one empty TV channel 

48
00:03:23,000 --> 00:03:30,000
and hops on next empty TV channel and repeats 

49
00:03:37,000 --> 00:03:41,000
Here is the block digram of receiver 

50
00:03:41,000 --> 00:03:45,000
it consist of differential QPSK demodulator 

51
00:03:45,000 --> 00:03:47,000
packet decoder and file sync.

52
00:03:47,000 --> 00:03:49,000 
Preamble removal is done off-line. 

53
00:03:49,000 --> 00:03:51,000
Let's see the receiver demo in action 

54
00:03:57,000 --> 00:04:02,000
Here is our receiver constellation and 

55
00:04:02,000 --> 00:04:04,000
now you can see complete TX and 

56
00:04:04,000 --> 00:04:08,000
RX set up in action. 

57
00:04:08,000 --> 00:04:16,000
This is our transmitter and this this is our receiver.



